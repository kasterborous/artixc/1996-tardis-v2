local PART={}
PART.ID = "inner_pillars"
PART.Name = "1996 inner_pillars"
PART.Model = "models/artixc/mcgannr/inner_pillars.mdl"
PART.AutoSetup = true
PART.Collision = true
//Bigdoor
local dooropen = ""
local doorpos = Vector(0,0,0)
local PART={}
PART.ID = "big_doors"
PART.Name = "1996 big doors"
PART.Model = "models/artixc/mcgannr/bigdoors.mdl"
PART.AutoSetup = true
PART.Animate = true
PART.AnimateSpeed = 0.6
PART.Collision = true
PART.Sound = dooropen
PART.SoundPos = doorpos
if SERVER then
    function PART:Collide()
        self:SetCollisionGroup(COLLISION_GROUP_NONE)
    end

    function PART:DontCollide()
        self:SetCollisionGroup(COLLISION_GROUP_WORLD)
    end

    function PART:Use()
        if ( self:GetOn() ) then
            self:EmitSound( Sound( "insertsoundbruh" ))
            self:Collide( true )
        else
            self:EmitSound( Sound( "insertsoundbruh" ))
            self:DontCollide( true )
        end
    end
end

TARDIS:AddPart(PART)