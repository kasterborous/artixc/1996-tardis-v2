local PART={}
PART.ID = "mcgannrot"
PART.Name = "1996 rotor"
PART.Model = "models/artixc/mcgann/rotor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true

if CLIENT then

    function PART:Initialize()
    self.timerotor={}
    self.timerotor.pos=0
    self.timerotor.mode=1
    end

    function PART:Think()

        local exterior=self.exterior
        local interior=self.interior

        if (self.timerotor.pos>0 and not exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) or (exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) then
                if self.timerotor.pos==0 then
                    self.timerotor.pos=1
                elseif self.timerotor.pos==1 and (exterior:GetData("flight") or exterior:GetData("teleport") or exterior:GetData("vortex")) then
                    self.timerotor.pos=0
                end

            self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.32 )
            self:SetPoseParameter( "motion", self.timerotor.pos )
        end
    end
end

TARDIS:AddPart(PART)