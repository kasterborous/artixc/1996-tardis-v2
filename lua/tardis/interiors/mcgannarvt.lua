-- 1996 Tardis

local T = {}
T.Base = "base"
T.Name = "1996 TARDIS v2"
T.ID = "mcgannavt"
T.Interior = {
    RequireLightOverride = true,
    Model = "models/artixc/mcgannr/basic_room.mdl",
    IdleSound = {
        {
            path = "artixc/mcgann/interioramb.wav",
            volume = 0.2
        },
    },
    LightOverride = {
        basebrightness = 0.027,
        nopowerbrightness = 0.003
    },
    Light={
        color=Color(66,144,196),
        pos=Vector(0,0,84),
        brightness=0.01
  
  },
  Lights={
    {
    color=Color(252,241,180),
    pos=Vector(0,0,300),
    brightness=0.5
    },
    {
    color=Color(252,241,180),
    pos=Vector(280,-140,65),
    brightness=0.5
    }

},
Lamps = {
    --BEGIN 6 CONSOLE LIGHTS
    six = {
        color = Color(255, 255, 255),
        texture = "effects/flashlight/hard",
        fov = 25,
        distance = 300,
        brightness = 1.5909091234207,
        pos = Vector(0, 30, 250),
        ang = Angle(75, -270, 0),
    },
    {
        color = Color(255, 255, 255),
        texture = "effects/flashlight/hard",
        fov = 25,
        distance = 300,
        brightness = 1.5909091234207,
        pos = Vector(25, 15, 250),
        ang = Angle(75, -330, 0),
    },
    {
        color = Color(255, 255, 255),
        texture = "effects/flashlight/hard",
        fov = 25,
        distance = 300,
        brightness = 1.5909091234207,
        pos = Vector(25, -15, 250),
        ang = Angle(75, -390, 0),
    },
    {
        color = Color(255, 255, 255),
        texture = "effects/flashlight/hard",
        fov = 25,
        distance = 300,
        brightness = 1.5909091234207,
        pos = Vector(0, -30, 250),
        ang = Angle(75, -450, 0),
    },
    {
        color = Color(255, 255, 255),
        texture = "effects/flashlight/hard",
        fov = 25,
        distance = 300,
        brightness = 1.5909091234207,
        pos = Vector(-25, -15, 250),
        ang = Angle(75, -510, 0),
    },
    {
        color = Color(255, 255, 255),
        texture = "effects/flashlight/hard",
        fov = 25,
        distance = 300,
        brightness = 1.5909091234207,
        pos = Vector(-25, 15, 250),
        ang = Angle(75, -570, 0),
    },
    --END 6 CONSOLE LIGHTS
    --BEGIN EXTRAS
    {
        color = Color(255, 246, 194),
        texture = "effects/flashlight/soft",
        fov = 115,
        distance = 1000,
        brightness = 0.5,
        pos = Vector(152.9, 150, 250),
        ang = Angle(30, 90, 0),
    },
    {
        color = Color(255, 246, 194),
        texture = "effects/flashlight/soft",
        fov = 80,
        distance = 1000,
        brightness = 0.5,
        pos = Vector(-270.9, -120, 390),
        ang = Angle(30, 210, 0),
    },
    {
        color = Color(4, 16, 108),
        texture = "effects/flashlight/hard",
        fov = 80,
        distance = 500,
        brightness = 1.5,
        pos = Vector(-120, 215, 22),
        ang = Angle(-20, -60, 0),
    },
    {
        color = Color(4, 16, 108),
        texture = "effects/flashlight/hard",
        fov = 80,
        distance = 500,
        brightness = 1.5,
        pos = Vector(-120, -215, 22),
        ang = Angle(-20, 60, 0),
    },
},
Portal={
    pos=Vector(152.9,509.5,71.2),
    ang=Angle(0,-90,0),
    width=45,
    height=90
},
Fallback={
    pos=Vector(153.591,473.41,26.70),
    ang=Angle(0,90,0),
},
    Screens = {
        {
            pos = Vector(44.5, -6.5, 39),
            ang = Angle(0, 84.5, 91),
            width = 227.75,
            height = 140,
            visgui_rows = 2,
            power_off_black = true
        }
    },
    Sequences = "default_sequences",
    Parts = {
        door = {
            model = "models/artixc/mcgann/intdoor.mdl",
            posoffset = Vector(23.3,0,-49.5),
            angoffset = Angle(0,180,0)
        },
        mcgannrot=true,
        consoleholder=true,
        console_gutters=true,
        enterance_stuff=true,
        corridor_wall=true,
        inner_pillars=true,
        bookwall=true,
        redwall=true,
        whitepannels=true,
        consolebase = {pos = Vector(0, 0, -0.37), ang = Angle(0, 0, 0), },
        mcganntoprol=true,
        mcgannmidrol=true,
        mcgannbot1rol=true,
        mcgannbot2rol=true,
        mcgannbot3rol=true,
        mcgannbot4rol=true,
        mcgannbot5rol=true,
        mcgannbot6rol=true,
        glowcon=true,
        mcghandbreak = {pos = Vector(0, 0, 0), ang = Angle(0, 0, 0), },
        mcgannwind = {pos = Vector(0, 0, 0), ang = Angle(0, 0, 0), },
        mcgannmafl=true,
        mcganntdial=true,
        sidedoor=true,
        big_doors=true,
    },
    Controls = {
        mcghandbreak = "teleport",
        mcgannwind = "flight",
        default_screen = nil,
        mcganntoprol = "coordinates",
        mcgannmafl = "thirdperson",


    },
    Tips = {},
    -- Interior.Tips are deprecated; should be deleted when the extensions update and
    -- replace with Interior.CustomTips, Interior.PartTips and Interior.TipSettings
    TipSettings = {
        view_range_min = 70,
        view_range_max = 90,
    },
    CustomTips = {
        --{ text = "Example", pos = Vector(0, 0, 0) },
    },
    PartTips = {

        mcghandbreak = {pos = Vector(19.374, 8.537, 55.021), down = true},
        mcgannwind = {pos = Vector(-27.099, -8.766, 50.625), down = true},
        mcganntoprol = {pos = Vector(19.436, -7.768, 52.619), down = true},
        mcgannmafl = {pos = Vector(7.89, -13.923, 55.648), down = true},

    },
    Seats = {
        {
            pos = Vector(130, -96, -30),
            ang = Angle(0, 40, 0)
        },
        {
            pos = Vector(125, 55, -30),
            ang = Angle(0, 135, 0)
        }
    },
    BreakdownEffectPos = Vector(0, 0, 40),
}

T.Exterior={
    Model="models/artixc/mcgann/exterior.mdl",
    Mass=2000,
    DoorAnimationTime = 0.9,
    Portal={
        pos=Vector(23.9,0,49.5),
        ang=Angle(0,0,0),
        width=45,
        height=92
    },
    Fallback={
        pos=Vector(35,0,5.5),
        ang=Angle(0,0,0)
    },
    Light={
        enabled=true,
        pos=Vector(0,0,115),
        color=Color(80,80,255)
    },
    Sounds={
        Teleport={
            demat="artixc/mcgann/demat.wav",
            mat="artixc/mcgann/mat.wav"
        },
        Lock="doctorwho1200/baker/lock.wav", --TEMP 
        Door={
            enabled=true,
            open="artixc/mcgann/mcgannopena.wav", 
            close="doctorwho1200/baker/doorext_close.wav" --TEMP 
        },
        FlightLoop="artixc/mcgann/flight_loop.wav"
    },
    Parts={
                door={
                        model="models/artixc/mcgann/intdoor.mdl",
                        posoffset=Vector(-23.3,0,-49.5),
                        angoffset=Angle(0,0,0),
                },
        vortex={
            model="models/doctorwho1200/mcgann/1996timevortex.mdl",--TEMP 
            pos=Vector(0,0,50),
            ang=Angle(0,0,0),
            scale=10
        }
    },
    Teleport = {
        SequenceSpeed = 0.60,
        SequenceSpeedFast = 0.935,
        DematSequence = {
            255,
            200,
            150,
            100,
            70,
            50,
            20,
            0
        },
        MatSequence = {
            0,
            20,
            50,
            100,
            150,
            180,
            255
        }
    }
}

TARDIS:AddInterior(T)